import java.util.Scanner;

public class Mathe {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner (System.in);
		double zahlx = liesZahl(myScanner, "Was möchtest du quadrieren?");
		double ergebnis = quadrat(zahlx);
		ausgabe(ergebnis);
		myScanner.close();
	}
	
	public static double liesZahl(Scanner ms,String text) {
		System.out.println(text);
		double zahl = ms.nextDouble();
		return zahl;
		
	}
	
	public static double quadrat(double x) {
		double result = x * x;
		return result;
	}
	
	public static void ausgabe(double result) {
		System.out.println("Das Ergebnis lautet: " + result);
	}

}
