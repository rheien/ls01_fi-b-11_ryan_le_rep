public class Lotto{
    public static void main(String[] args) {
        int[] ticket={3 , 7 , 12 , 18 , 37 , 42};
        int[] zahl={12 , 13 , 14 , 42};

        //Die Länge von Liste ticket
        int laenge=ticket.length;

        //Ausgabe des Tickets
        System.out.print("[ ");
        for(int i:ticket){ System.out.print(" "+ i +" "); }
        System.out.print(" ] \n \n");

        //Schleife zur Überprüfung der Zahlen
        for (int j=0; j<zahl.length; j++){

            //eine boolean Liste mit false
            boolean[] zahl1=new boolean[laenge];
            for (int k=0; k<laenge; k++){
                if(zahl[j]==ticket[k]){
                    
                    //setzt auf true wenn Zahl im Ticket befindet
                    zahl1[k]=true;
                }
            }

            //Ausgabe für die Überprüfung
            //Laufindex für die boolean Liste
            int a=0;
            for (boolean l: zahl1){
                if (l){
                    System.out.printf("Die Zahl %d ist in der Ziehung enthalten.%n",zahl[j]);
                }
                else if(l==false){
                    a++;
                    if(a==laenge){
                        System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.%n",zahl[j]);
                    }
                }
            }
        }
    }
}