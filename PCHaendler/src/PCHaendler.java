import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		String artikel = liesString(myScanner, "was möchten Sie bestellen?");
		int anzahl = liesInt(myScanner,"Geben Sie die Anzahl ein:");
		double preis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
		double mwst = liesDouble (myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl,preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis,mwst); 
		
		rechnungausgeben(artikel,anzahl,nettogesamtpreis,bruttogesamtpreis,mwst);
		myScanner.close();
	}
	
	//Eingabe des Artikelnamen
	public static String liesString(Scanner ms, String text) {
		System.out.println(text);
		String artikel = ms.next();
		return artikel;
	}

	//Eingabe der Artikelanzahl
	public static int liesInt(Scanner ms,String text) {
		System.out.println(text);
		int anzahl = ms.nextInt();
		return anzahl;
	}
	 
	//Eingabe eines Double für den Nettopreis oder MWST
	public static double liesDouble(Scanner ms, String text) {
		System.out.println(text);
		double preisodermwst = ms.nextDouble();
		return preisodermwst;
	}
	 
	//Berechnung des Gesamtreises
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double netto = anzahl * nettopreis;
		return netto;
	}
	
	//Berechnung Preis mit MWST
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double brutto = nettogesamtpreis * (1 + mwst / 100);
		return brutto;
	}
	
	//Ausgabe Gesamtrechnung
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	 
	
	
	//Aufgabe 3
	//Errechnet den Ersatzwiderstand der Reihenschaltung R1 und R2
	public static double reihenschaltung(double r1, double r2) {
		double rgesamt = r1 + r2;
		return rgesamt;
	}
	
	//Aufgabe 4
	//Errechnet den Ersatzwiderstand der Parallelschaltung R1 und R2
	public static double parallelschaltung(double r1, double r2) {
		double rgesamt = (r1 * r2)/(r1 + r2);
		return rgesamt;
	}
}