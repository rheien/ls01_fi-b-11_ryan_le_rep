import java.util.Scanner;

/*
 * Anpassung des Projektes Fahrkartenautomat.java mit vier Methoden
 * fahrkartenbestellungErfassen
 * fahrkartenBezahlen
 * fahrkartenAusgeben
 * rueckgeldAusgeben
 */

public class FahrkartenautomatmitMethoden {
	public static void main(String[] args) {
	      
	    double zuZahlenderBetrag; 
	    double eingezahlterGesamtbetrag;
		boolean schleife=true;

		System.out.println("Fahrkartenbestellvorgang:");
		for (int i = 0; i < 25; i++){
	    	System.out.print("=");
	    	warte(50);
	    }

		//Endlosschleife
		//--------------
		while (schleife==true){
			//Eingabe des Ticketpreises und der Anzahl der Tickets
			zuZahlenderBetrag=fahrkartenbestellungErfassen();

			//Speichert die eingezahlten Beträge und gibt die noch zu bezahlenden Beträge aus
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			
			//Ausgabe des Restgeldes und der Tickets
			fahrkartenAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}
	}

	//Methode für zuZahlenderBetrag
	// ----------------------------
	public static double fahrkartenbestellungErfassen() {
		double preis,fahrkarten;
		double zwischensumme=0;
		do{
			fahrkarten=wunschFahrkarte(tarif());
			if (fahrkarten==0){
				return zwischensumme;
			}
			preis= ticketAnzahl("Anzahl der Tickets: ",fahrkarten);
			zwischensumme+=preis;
			System.out.printf("%nZwischensumme: %.2f €",zwischensumme);
		}while(fahrkarten!=0);
		return zwischensumme;
	}


	//Methode für feste Ticketpreise
    // -----------------------
	public static double wunschFahrkarte(int ticket){
		double[] ticketpreis={0 , 2.90 , 3.30 , 3.60 , 1.90 , 8.60 , 9.00 , 9.60 , 23.50 , 24.30 , 24.90};
		return ticketpreis[ticket];
	}

	//Methode für Tickettarife
    // -----------------------
	public static int tarif() {
		Scanner m = new Scanner(System.in);
		String[] bezeichnung={"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};

		System.out.println("\n \nWählen Sie Ihre Wunschfahrkarte für Berlin AB aus:\n");
		for (int i=0; i<bezeichnung.length;i++){
			System.out.printf("  %s [%.2f]    (%d)%n",bezeichnung[i],wunschFahrkarte(i+1),(i+1));
		}
		System.out.println(" Bezahlen (0)\n");
						
		int ticket=0;

		//Schleife für die richtige Eingabe des Tarifs
		do {
			System.out.print("Ihre Wahl: ");
			ticket=m.nextInt();

			//Auswahl von den drei Tarifen
			if (0<=ticket && ticket<=10){
				return ticket;
			}
			else{
				System.out.println(" >>falsche Eingabe<<");
				ticket=-1;
			}
		} while (ticket==-1);
		return (ticket+1);
	}

	//Methode für Ticketanzahl
    // -----------------------
	public static double ticketAnzahl(String text,double betrag){
		Scanner ms = new Scanner(System.in);
		byte ticket;
		do{
			ticket=0;
			System.out.print(text);
			ticket=ms.nextByte();
			if (ticket<1 || 10<ticket){
				System.out.println(" >>Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		} while (ticket<1 || 10<ticket) ;
		return ticket*betrag;
	}

	//Methode für Eingabe der eingeworfenen Münzen
	//-------------------------------------------
	public static double eingabe(String text) {
		Scanner m = new Scanner(System.in);
		System.out.print(text);
		return m.nextDouble();
	}

	
    // Geldeinwurf
    // -----------
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	    double eingezahlterGesamtbetrag = 0.0;
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
	    	System.out.printf("Noch zu zahlen: %.2f EURO%n",zuZahlenderBetrag-eingezahlterGesamtbetrag);
			double eingeworfeneMünze = eingabe("Eingabe (mind. 5Ct, höchstens 2 Euro): ");				
			if (eingeworfeneMünze<0 || 2<eingeworfeneMünze){
				System.out.println("Falsche Eingabe!");
			}
			else{
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			}
	    }	
	    return eingezahlterGesamtbetrag;
	}
	
	
    // Fahrscheinausgabe
    // -----------------
	public static void fahrkartenAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
	    System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++){
	    	System.out.print("=");
	    	warte(250);
	    }
	    System.out.println("\n\n");
	    rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);	    
	    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	    				   "vor Fahrtantritt entwerten zu lassen!\n"+
	    				   "Wir wünschen Ihnen eine gute Fahrt.\n");	    
	}
	
	// Programmpause
	// -----------------
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
	}
	
	
    // Rückgeldberechnung
    // -------------------------------
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag;
	    rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    if(rückgabebetrag > 0.0) {
	    	System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",rückgabebetrag);
	    	System.out.println("wird in folgenden Münzen ausgezahlt:");

	    	while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	    	{
	    		muenzeAusgeben(2," Euro");
	    		rückgabebetrag -= 2.0;
	    	}
	    	while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	    	{
	    		muenzeAusgeben(1," Euro");
	    		rückgabebetrag -= 1.0;
	    	}
	    	while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	    	{
	    		muenzeAusgeben(50," Cent");
	    		rückgabebetrag -= 0.5;
	    	}
	    	while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.2) // 20 CENT-Münzen
	    	{
	    		muenzeAusgeben(20," Cent");
	    		rückgabebetrag -= 0.2;
	    	}
	    	while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.1) // 10 CENT-Münzen
	    	{
	    		muenzeAusgeben(10," Cent");
	    		rückgabebetrag -= 0.1;
	    	}
	    	while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
	    	{
	    		muenzeAusgeben(5," Cent");
	    		rückgabebetrag -= 0.05;
	    	}
	    }
	}
	
	// Ausgabe
    // -------------------------------
	public static void muenzeAusgeben(int betrag, String Einheit) {
		System.out.println(betrag + Einheit);
	}
}