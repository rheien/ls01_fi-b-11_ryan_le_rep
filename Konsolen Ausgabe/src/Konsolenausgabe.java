
public class Konsolenausgabe {

	public static void main(String[] args) {
		System.out.println("Hallo! \nWie geht es dir?");
		System.out.println("Alle \b klar");
		System.out.println("Mein Tag ist \t Hammer");
		System.out.println("bla\fholy");
		
		String blub="Java-Programm";
		int i=123;
		double d=12.9374856;
		
		System.out.printf("\n|%s|\n", blub);
		System.out.printf( "|%20s|\n", blub );
		System.out.printf( "|%-20s|\n", blub );
		System.out.printf( "|%5s|\n", blub );
		System.out.printf( "|%.4s|\n", blub );
		System.out.printf( "|%20.4s|\n", blub );
		
		System.out.printf("%d %d\n", i,i);
		System.out.printf( "|%5d| |%5d|\n" ,i, -i);
		System.out.printf( "|%-5d| |%-5d|\n" , i, -i);
		System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);
		System.out.printf( "|%05d| |%05d|\n", i, -i);
		System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );
		System.out.printf( "|%08x| |%#x|\n\n", 0xabc, 0xabc );
		
		
		System.out.printf( "|%f| |%f|\n" ,d,-d);
		System.out.printf( "|%.2f| |%.2f|\n" ,d,-d);
		System.out.printf( "|%10f| |%10f|\n" ,d,-d);
		System.out.printf( "|%10.2f| |%10.2f|\n" , d, -d);
		System.out.printf( "|%010.2f| |%010.2f|\n", d, -d);
		
	}

}
