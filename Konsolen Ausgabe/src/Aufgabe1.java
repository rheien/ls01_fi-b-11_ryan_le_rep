
public class Aufgabe1 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		String Wetter="Winter";
		
		System.out.print("Der \""+Wetter+"\" ist kalt. \n");
		System.out.print( "Schnee im "+Wetter+".");
		
		//print gibt einfach aus
		//println setzt automatisch einen Zeilenumbruch
		
		
		
		//Aufgabe 2
		System.out.println("\n\n\n");
		String s="*************";
		
		System.out.printf("\n%7.1s\n",s);
		System.out.printf("\n%8.3s\n",s);
		System.out.printf("\n%9.5s\n",s);
		System.out.printf("\n%10.7s\n",s);
		System.out.printf("\n%11.9s\n",s);
		System.out.printf("\n%12.11s\n",s);
		System.out.printf("\n%s\n",s);
		System.out.printf("\n%8.3s\n",s);
		System.out.printf("\n%8.3s",s);
		
		//Aufageb 3
		System.out.printf("\n\n\n");
		
		double d1=22.4234234;
		double d2=111.2222;
		double d3=4.0;
		double d4=1000000.551;
		double d5=97.34;
		
		
		System.out.printf("\n%.2f",d1);
		System.out.printf("\n%.2f",d2);
		System.out.printf("\n%.2f",d3);
		System.out.printf("\n%.2f",d4);
		System.out.printf("\n%.2f",d5);
	
		
	}

}
