
public class Uebung2 {

	public static void main(String[] args) {
		// Aufgabe 1
		
		String s="*";
		
		System.out.printf("%4s"+"%s\n",s,s);
		System.out.printf("%s"+"%7s\n",s,s);
		System.out.printf("%s"+"%7s\n",s,s);
		System.out.printf("%4s"+"%s\n",s,s);

		
		
		
		//Aufgabe 2
		String platz1="  ";
		String platz2=" ";
		
		
		System.out.printf("\n\n\n");
		System.out.printf("0! %s = %17s =%-3s1\n",platz1,platz2,platz2);
		System.out.printf("1! %s = 1 %15s =%-3s1\n",platz1,platz2,platz2);
		System.out.printf("2! %s = 1 * 2 %11s =%-3s2\n",platz1,platz2,platz2);
		System.out.printf("3! %s = 1 * 2 * 3 %7s =%-3s6\n",platz1,platz2,platz2);
		System.out.printf("4! %s = 1 * 2 * 3 * 4 %3s =%-2s24\n",platz1,platz2,platz2);
		System.out.printf("5! %s = 1 * 2 * 3 * 4 * 5 =%s120\n",platz1,platz2);
		
		
		//Aufgabe 3
		System.out.printf("\n\n\n");
		
		String tempe0="0";
		String tempe1="10";
		String tempe2="20";
		String tempe3="30";
		String Fahren="Fahrenheit";
		String Cel="Celsius";
		
		double d1=-28.8889;
		double d2=-23.3333;
		double d3=-17.7778;
		double d4=-6.6667;
		double d5=-1.1111;
		
		System.out.printf("%-12s|%10s\n",Fahren,Cel);
		System.out.printf("------------------------\n");
		System.out.printf("-%-11s|%10.2f \n",tempe2,d1);
		System.out.printf("-%-11s|%10.2f\n",tempe1,d2);
		System.out.printf("+%-11s|%10.2f\n",tempe0,d3);
		System.out.printf("+%-11s|%10.2f\n",tempe2,d4);
		System.out.printf("+%-11s|%10.2f\n",tempe3,d5);
	}

}
