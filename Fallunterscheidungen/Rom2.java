import java.util.Scanner;

public class Rom2 {
    public static void main(String []args) {
        Scanner tast=new Scanner(System.in);
        
        String[] roemisch=eingabe(tast,"Geben Sie römische Zeichen (I, V, X, L, C, D, M) ein: ");
        ergebnis(rechnung (roemisch));
        tast.close();
    }

    public static void ergebnis(int result){
        if (result==0){
            System.out.println("Falsche Eingabe. Man kann es nicht in Dezimalzahlen umwandeln!");
        }
        else{
            System.out.println("Die Dezimalzahl lautet: "+result);
        }
    }

    //Eingabe umkehren und in einem Array speichern
    //---------------------------------
    public static String[] eingabe(Scanner ms,String text){
        System.out.println(text);

        //Alle Eingaben werden in Großbuchstaben übergeben
        String roemisch=ms.next().toUpperCase();
        if (regeln2(roemisch)){
            
            //Methode zu umkehren
            StringBuilder sb=new StringBuilder(roemisch);
            sb.reverse();
            return sb.toString().split("");
        }
        else{
            roemisch="";
            return roemisch.split("");
        }
    }

    //Bestimmte Zeichenfolgen erlaubt
    //---------------------------------
    public static boolean regeln2(String roemisch) {
        if (roemisch.contains("IIII") ||
        roemisch.contains("XXXX") ||
        roemisch.contains("CCCC") ||
        roemisch.contains("MMMM") ||
        roemisch.contains("VV") ||
        roemisch.contains("LL") ||
        roemisch.contains("DD") ||
        roemisch.contains("IIV") ||
        roemisch.contains("IIX") ||
        roemisch.contains("XXL") ||
        roemisch.contains("XXC") ||
        roemisch.contains("CCD") ||
        roemisch.contains("CCM")){
            return false;
        }
        return true;
    }

    //Eingabe umkehren und in einem Array speichern
    //---------------------------------
    public static int rechnung(String[] eingabe){
        int dezimal=0;
        int hintereZahl=0;

        //Index sind die Inhalte im Array
        for ( String zeichen : eingabe){

            //Rechnung in Dezimal
            //mit Blick auf rechte Zahl
            dezimal=regeln(auslesen(zeichen),hintereZahl,dezimal);
            hintereZahl=auslesen(zeichen);
            if (dezimal==0){
                return 0;
            }
        }
        return dezimal;
    }

    //zwei Regeln zum Addieren oder Subtrahieren
    //---------------------------------
    public static int regeln(int zahl, int hintereZahl, int letzteZahl) {
        if (hintereZahl>zahl){
            if (zahl==1 && (hintereZahl==5 || hintereZahl==10)){
                return letzteZahl-zahl;
            }
            else if(zahl==10 && (hintereZahl==50 || hintereZahl==100)){
                return letzteZahl-zahl;
            }
            else if(zahl==100 && (hintereZahl==500 || hintereZahl==1000)){
                return letzteZahl-zahl;
             }
            else{
                //Falsche Eingabe
                return 0;
            }
        }
        else{
            return letzteZahl+zahl;
        }
    }

    //Methode zum Auslesen der Zeichen
    //-----------------------------------------
    public static int auslesen(String zeichen){
        int a=0;
        switch(zeichen){
            case "I":
                a=1;
                break;
            case "V":
                a=5;
                break;
            case "X":
                a=10;
                break;
            case "L":
                a=50;
                break;
            case "C":
                a=100;
                break;
            case "D":
                a=500;
                break;
            case "M":
                a=1000;
                break;
        }
        return a; 
    }   
}