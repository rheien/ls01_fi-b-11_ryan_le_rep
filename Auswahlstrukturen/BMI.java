import java.util.Scanner;

public class BMI {
    public static void main(String []args) {

        double result;
        char classification;

        Scanner tast=new Scanner(System.in);

        classification=vergleich(tast);
        result=berechnung(tast);
        ergebnis(result,classification);

        tast.close();
    }

    public static double berechnung(Scanner m){
        System.out.println("Gewicht in kg: ");
        double weight=m.nextDouble();
        System.out.println("Höhe in m: ");
        double height=m.nextDouble();
        
        double bmi;
        bmi=weight/(height*height);
        
        return bmi;
    }
    
    public static char vergleich(Scanner m){
        System.out.println("Geschlecht m oder w?");
        char sex=m.next().charAt(0);
        return sex;
    }

    public static void ergebnis(double result, char classification){
       if (classification=='w'){
            if (result<19.){
                System.out.println("Untergewicht");
            }
            else if (result > 24.){
                System.out.println("Übergewicht");
            }
            else{
                System.out.println("Normalgewicht");
            }
        }
        else if (classification=='m'){
            if (result<20){
                System.out.println("Untergewicht");
            }
            else if (result > 25){
                System.out.println("Übergewicht");
            }
            else{
                System.out.println("Normalgewicht");
            }
        }    
        
    }

}
