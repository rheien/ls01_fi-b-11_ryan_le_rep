import java.util.Scanner;

public class Treppen{
    public static void main(String []args) {
        Scanner tast=new Scanner(System.in);
        int height=eingabe(tast,"Bitte geben Sie die Treppenhöhe ein: ");
        int length=eingabe(tast,"Bitte geben Sie die Treppenbreite ein: ");
        ausgabe(height,length);
        tast.close();
    }

    //Methode Eingabe für die Treppe
    //-----------------------------------
    public static int eingabe(Scanner ms,String text) {
        System.out.print(text);
        return ms.nextInt();
    }

    //Methode Treppenform
    //-----------------------------------
    public static void ausgabe(int height,int length){
    
        //oben links wird angefangen
        for (int i=0;i<height+1;i++){
            String ergebnis="";                         //String zurücksetzen
            for (int k=(height-i)*length;k>0;k--){      //einfügen der Leerzeichen
                ergebnis+=" " ;                         
            }
            for (int j=0;j<length*i;j++){               //einfügen der Sternchen
                ergebnis+="*";
            }
            
            System.out.println(ergebnis);
        }
    }
}



//int format=2*b*h;
//("%{0}s",format)