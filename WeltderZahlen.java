/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.10.2021
  * @author << Ryan Le >>
  */

  public class WeltDerZahlen {

    public static void main(String[] args) {
      
      /*  *********************************************************
      
           Zuerst werden die Variablen mit den Werten festgelegt!
      
      *********************************************************** */
      // Im Internet gefunden ?
      // Die Anzahl der Planeten in unserem Sonnesystem                    
      byte anzahlPlaneten = 8 ;
      
      // Anzahl der Sterne in unserer Milchstraße
      long anzahlSterne = 4 * 10^11 ; //200 Mia.
      
      // Wie viele Einwohner hat Berlin?
      long bewohnerBerlin = 3645 * 10^3 ; //3.645 Mio.
      
      // Wie alt bist du?  Wie viele Tage sind das?
      
      short alterTage = 10757 ; //13.05.1992
      
      // Wie viel wiegt das schwerste Tier der Welt?
      // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm = 150000 ;   //Blauwal
      
      // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
      int flaecheGroessteLand = 17098242 ; //Russland
      
      // Wie groß ist das kleinste Land der Erde?
      
      float flaecheKleinsteLand = 0.44f ; //Vatikanstadt
      
      
      
      
      /*  *********************************************************
      
           Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
      
      *********************************************************** */
      
      System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
      
      System.out.println("Anzahl der Sterne: " + anzahlSterne);

      System.out.println("Anzahl Einwohner in Berlin: " + bewohnerBerlin);
      
      System.out.println("Anzahl Tage: " + alterTage);

      System.out.println("Schwerstes Tier: " + gewichtKilogramm);

      System.out.println("Größte Fläche: " + flaecheGroessteLand);

      System.out.println("Kleinste Fläche: " + flaecheKleinsteLand);
      
      
      System.out.println(" *******  Ende des Programms  ******* ");
      
    }
 }
  