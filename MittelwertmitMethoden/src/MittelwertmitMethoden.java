import java.util.Scanner;

public class MittelwertmitMethoden {
    public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

        double anzahl=eingabe(myScanner,"Wie viele Zahlen sollen addiert werden? ");
        double zahl=schleife(myScanner, anzahl);
        ausgabeMittelwert(rechnung(zahl,anzahl));

        myScanner.close();
    }

    //Ausgabe
    public static void ausgabeMittelwert(double m) {
        System.out.println("Mittelwert: " + m);
    }

    public static double schleife(Scanner m,double zahl1){
        double summe=0;
        double zahl2;
        while (zahl1>0){
            zahl2=eingabe(m,"Bitte geben Sie die Zahl ein: ");
            summe+=zahl2;
            zahl1--;
        }
        return summe;
    }

    public static double eingabe(Scanner m, String text) {
        System.out.print(text);
        return m.nextDouble();
    }

    public static double rechnung(double zahl1, double zahl2) {
        return zahl1/zahl2;
    }
}