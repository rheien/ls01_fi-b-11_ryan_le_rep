import java.util.Scanner;

public class Fakultät {

    public static void main(String []args) {
        Scanner tast=new Scanner(System.in);
        ausgabe(fakultätrechnung(eingabe(tast,"Die Fakultät von ")));
    }

    public static void ausgabe(String text) {
        System.out.println("Ergebnis ist "+text);
    }

    public static byte eingabe(Scanner ms, String text) {
        System.out.println(text);
        byte zahl = ms.nextByte();
        if (zahl>20){
            System.out.println("Die Zahl ist zu groß.");
            return eingabe(ms, text);
        }
        else{
            return zahl;
        }
    }

    public static String fakultätrechnung(int zahl){
        int ergebnis=1;
        do {
            ergebnis*=zahl;
            zahl--;
        } while (zahl>0);
        return Integer.toString(ergebnis);
    }
}