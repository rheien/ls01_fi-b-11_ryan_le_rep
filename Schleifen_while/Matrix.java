import java.util.Scanner;

public class Matrix {
    public static void main(String []args) {
        Scanner tast=new Scanner(System.in);

        matrix(tast,"Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");

        tast.close();
    }


    public static boolean regeln(int zahl,int eingabe) {
        int rest=zahl%eingabe;
        int quer=quersumme(zahl);
        
        //Null muss rausgenommen werden
        if (zahl==0){
            return false;
        }

        //Die Zahlen sind durch die Eingabe teilbar
        else if(rest==0){
            return true;
        }

        //Die Eingabe taucht in der Quersumme auf
        else if(quer==eingabe){     
            return true;
        }
        else{
            return false;
        }
    }

    public static int quersumme(int zahl) {
        if(zahl<=9){
            return zahl;
        }
        else{
            //rekursiv
            return zahl%10 + quersumme(zahl/10);
        }
    }

    public static void matrix(Scanner ms, String text) {
        System.out.print(text);
        int eingabe=ms.nextInt();
      
        if (2<=eingabe && eingabe<=9){
            
            int i=0 ; //Zeilenindex
            int j= 0; //Spaltenindex
            String ausgabe="";
            
            //Zeile wird festgehalten
            while (i<=9){

                //schaut jede Spalte an
                while (j<=9){
                    
                    //10ner Schritten
                    //10 11 12 13 14 15 16 17 18 19
                    //20 21 22 ...               29
                    if (regeln(i*10 + j,eingabe)==true){
                        ausgabe+=" * ";
                    }
                    else{
                        //spezielle Ausgabe für die erste Zeile
                        if(i==0){
                            ausgabe+=" "+ (i+j) +" ";
                        }

                        //Sonderfall:
                        //enthaltene Eingabe abfangen
                        else if (i==eingabe || j ==eingabe){
                            ausgabe+=" * ";
                        }
                        else{
                            //dei Zahlen normalen Zahlen ausgeben
                            ausgabe+=(i*10 + j)+" ";
                        }
                    }
                //Spaltenindex erhöhen    
                j++;
                }
            System.out.println(ausgabe);
            ausgabe="";
            j=0;                            //Spaltenindex zurücksetzen
            i++;                            //Zeilenindex erhöhen
            }

        }
        else{
            System.out.println("Invalide Eingabe.");
            matrix(ms,text);
        }
    }
}