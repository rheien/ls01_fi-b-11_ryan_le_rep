import java.util.Scanner;

public class ProbeDatentyp
{
    public static void main(String[]args)
    {
        Scanner myScanner=new Scanner(System.in);

        float zahl1=0.0f, zahl2=0.0f, summe=0.0f;
        int zahl3=0, zahl4=0;

        double zahlDouble;
        long zahlLong;
        byte zahlByte;
        short zahlShort;
        boolean wahrheitswert;
        char buchstabe;


        String name=" ";


        System.out.println("Das Programm addiert zwei Zahlen zusammen.");
        System.out.print("Bitte geben Sie die erste Zahl ein: ");
        zahl1=myScanner.nextFloat();

        System.out.print("Bitte geben Sie die zweite Zahl ein: ");
        zahl2=myScanner.nextFloat();

        summe=zahl1+zahl2;

        System.out.println(summe);

        System.out.println("Bitte geben Sie Ihren Namen ein: ");
        name=myScanner.next();
        System.out.println(name);

        System.out.println("Das Programm addiert zwei Zahlen zusammen.");
        System.out.print("Bitte geben Sie die erste Zahl ein: ");
        zahl3=myScanner.nextInt();

        System.out.print("Bitte geben Sie die zweite Zahl ein: ");
        zahl4=myScanner.nextInt();

        System.out.println(zahl3+zahl4);

        System.out.print("Double: ");
        zahlDouble=myScanner.nextDouble();
        System.out.println(zahlDouble);

        System.out.print("Short: ");
        zahlShort=myScanner.nextShort();
        System.out.println(zahlShort);

        System.out.print("Long: ");
        zahlLong=myScanner.nextLong();
        System.out.println(zahlLong);

        System.out.print("Byte: ");
        zahlByte=myScanner.nextByte();
        System.out.println(zahlByte);

        System.out.print("Boolean: ");
        wahrheitswert=myScanner.nextBoolean();
        System.out.println(wahrheitswert);

        System.out.print("Char: ");
        buchstabe=myScanner.next().charAt(0);
        System.out.println(buchstabe);

        System.out.print("Char: ");
        buchstabe=myScanner.next().charAt(3);
        System.out.println(buchstabe);

        myScanner.close();
    }
}